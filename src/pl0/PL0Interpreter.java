package pl0;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.InputMismatchException;


public class PL0Interpreter {

    private AST ast;
    private RAM ram;

    public PL0Interpreter(AST ast) {
        this.ast = ast;
        this.ram = new RAM();
    }

    public void interpret() {
        interpretBlock(ast.getRoot(), 0);
    }

    private void interpretBlock(BlockNode block, Integer level) {
        ConstNode node = block.getCons();

        // Add new segment
        ram.addSegment(block.getVarCount() + block.getConsCount(), level);

        // Write constants to RAM
        while (node != null) {
            ram.setValue(node.getEntry(), node.getValue());
            node = node.getNext();
        }

        // Interpret first block statement
        interpretStmt(block.getStmt(), level);

        // Remove last segment
        ram.removeSegment();
    }

    private void interpretStmt(StmtNode stmt, Integer level) {
        while (stmt != null) {
            switch(stmt.getType()){
            case ASSIGN:    // Assign a value to an identifier
                ram.setValue(stmt.getEntry(), interpretExpr(stmt.getExpr()));
                break;
            case CALL:      // Call a procedure
                if (ast.getProcedures().keySet().contains(stmt.getName()))
                    interpretBlock(ast.getProcedures().get(stmt.getName()), stmt.getEntry().getLevel());
                break;
            case INPUT:      // Read an integer from standard input
                Scanner input = new Scanner(System.in);
                try {
                    System.out.println("Input: ");
                    ram.setValue(stmt.getEntry(), input.nextInt());
                } catch (Exception e) {
                    System.out.println("===[ Input error, only integers allowed ]===");
                    System.exit(0);
                }
                break;
            case OUTPUT:     // Write an expression to standard output
                System.out.println("Output: " + interpretExpr(stmt.getExpr()));
                break;
            case IF:        // If statement
                if (interpretExpr(stmt.getExpr()) != 0)
                    interpretStmt(stmt.getSub(), level);
                break;
            case WHILE:     // While statement
                while (interpretExpr(stmt.getExpr()) != 0)
                    interpretStmt(stmt.getSub(), level);
                break;
            case DEBUG:     // Print a ram dump
                System.out.println();
                System.out.println("===[ RAM DUMP ]===");
                System.out.println(ram);
                break;
            }

            stmt = stmt.getNext();
        }
    }

    private Integer interpretExpr(ExprNode expr) {
        switch (expr.getType()) {
        case ODD:       // ODD
            return interpretExpr(expr.getLeft()) % 2;
        case EQ:        // Equals
            return (interpretExpr(expr.getLeft()) == interpretExpr(expr.getRight()) ? 1 : 0);
        case NE:        // Not euals
            return (interpretExpr(expr.getLeft()) != interpretExpr(expr.getRight()) ? 1 : 0);
        case LT:        // Less than
            return (interpretExpr(expr.getLeft()) < interpretExpr(expr.getRight()) ? 1 : 0);
        case LE:        // Less equals
            return (interpretExpr(expr.getLeft()) <= interpretExpr(expr.getRight()) ? 1 : 0);
        case GT:        // Greater than
            return (interpretExpr(expr.getLeft()) > interpretExpr(expr.getRight()) ? 1 : 0);
        case GE:        // Greater equals
            return (interpretExpr(expr.getLeft()) >= interpretExpr(expr.getRight()) ? 1 : 0);
        case CS:        // Change sign
            return -interpretExpr(expr.getLeft());
        case IDENT:     // Return a value of an identifier
            return ram.getValue(expr.getEntry());
        case NUMBER:    // Return a number
            return expr.getValue();
        case ADD:       // Addition
            return interpretExpr(expr.getLeft()) + interpretExpr(expr.getRight());
        case SUB:       // Subtraction
            return interpretExpr(expr.getLeft()) - interpretExpr(expr.getRight());
        case MUL:       // Multiplication
            return interpretExpr(expr.getLeft()) * interpretExpr(expr.getRight());
        case DIV:       // Division
            return interpretExpr(expr.getLeft()) / interpretExpr(expr.getRight());
        default:        // Return null if no expression type match
            return null;
        }
    }

    public static void main(String args[]) {
        if (args.length < 1) {
            System.out.println("usage:");
            System.out.println("\tjava -jar PL0Interpreter.jar [source file]"); // TODO
            System.exit(0);
        }

        try {
            FileInputStream input = new FileInputStream(args[0]);
            Parser parser = new Parser(input);
            parser.parse();

            PL0Interpreter interpreter = new PL0Interpreter(parser.ast);
            interpreter.interpret();
        } catch (FileNotFoundException e) {
            System.out.println("===[ File '" + args[0] + "' not found  ]===");
        } catch (ParseException e) {
            System.out.println("===[ Syntax error ]===");
            System.out.println(e.getMessage());
        }

        System.exit(0);
    }

}