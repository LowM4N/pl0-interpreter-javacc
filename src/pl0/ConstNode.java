package pl0;


public class ConstNode {
    private String name;
    private Integer value;
    private SymbolTableEntry entry;
    private ConstNode next;

    public ConstNode(String name, Integer value, SymbolTableEntry entry) {
        this.name = name;
        this.value = value;
        this.entry = entry;
        this.next = null;
    }
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setValue(Integer value) {
		this.value = value;
	}

    public void setEntry(SymbolTableEntry entry) {
        this.entry = entry;
    }
	
    public void setNext(ConstNode next) {
        this.next = next;
    }
	
	public String getName() {
		return name;
	}
	
	public Integer getValue() {
		return value;
	}

    public SymbolTableEntry getEntry() {
        return entry;
    }

    public ConstNode getNext() {
        return next;
    }

    public String toString() {
        String s = "ConstNode: " + name + ", Value: " + value;
        if (next != null)
            return s + ", Next: " + next.name;
        return s;
    }
}
