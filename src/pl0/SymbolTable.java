package pl0;

import java.util.TreeMap;


public class SymbolTable {

    private Integer level;
    private TreeMap<Integer, TreeMap<String, SymbolTableEntry>> content;
    
    public SymbolTable() {
    	level = -1;
    	content = new TreeMap<Integer, TreeMap<String, SymbolTableEntry>>();
    }

    public void levelUp() {
    	content.put(++level, new TreeMap<String, SymbolTableEntry>());
    }

    public void levelDown() {
    	content.remove(level--);
    }

    public Integer insert(String name, SymbolTableEntry.Type type) {
    	if (content.get(level).keySet().contains(name))
            return -1; // Element exists
    	
    	Integer offset = content.get(level).size();
    	SymbolTableEntry entry = new SymbolTableEntry(level, offset, type);
    	content.get(level).put(name, entry);

        return 0; // OK
    }

    public Integer lookup(String name, SymbolTableEntry.Type type, SymbolTableEntry entry) {
        for (Integer i = this.level; i >= 0; i--) {
            TreeMap<String, SymbolTableEntry> map = content.get(i);

            if (map.keySet().contains(name)) {
                SymbolTableEntry e = map.get(name);

                entry.setLevel(level - i); // Calculate delta
                entry.setOffset(e.getOffset());
                entry.setType(e.getType());

                if (e.getType() != type)
                    return -1; // Wrong type

                return 0; // Element found
            }
        }

        return -2; // Element not found
    }
    
    public void print() {
    	System.out.println("Symbol Table Content:");
    	for (Integer level : content.keySet()) {
    		TreeMap<String, SymbolTableEntry> map = content.get(level);
    		System.out.println("Level: " + level);
    		
    		for (String key : map.keySet()) {
    			System.out.println("\tName: " + key);
                System.out.println("\t\t" + map.get(key));
    		}
    	}
    }

}
