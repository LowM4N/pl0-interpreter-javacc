package pl0;


public class BlockNode {

    private String name;
    private Integer varCount;
    private Integer consCount;
    private ConstNode cons;
    private BlockNode sub;
    private BlockNode next;
    private StmtNode stmt;

    public BlockNode(ConstNode cons, Integer varCount, Integer consCount, BlockNode sub, StmtNode stmt) {
        this.name = null;
        this.next = null;
        this.cons = cons;
        this.varCount = varCount;
        this.consCount = consCount;
        this.sub = sub;
        this.stmt = stmt;
    }
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setVarCount(Integer varCount) {
		this.varCount = varCount;
	}

    public void setConsCount(Integer consCount) {
        this.consCount = consCount;
    }
	
	public void setCons(ConstNode cons) {
		this.cons = cons;
	}
	
	public void setSub(BlockNode sub) {
		this.sub = sub;
	}
	
	public void setNext(BlockNode next) {
		this.next = next;
	}
	
	public void setStmt(StmtNode stmt) {
		this.stmt = stmt;
	}
	
	public String getName() {
		return name;
	}
	
	public Integer getVarCount() {
		return varCount;
	}

    public Integer getConsCount() {
        return consCount;
    }
	
	public ConstNode getCons() {
		return cons;
	}
	
	public BlockNode getSub() {
		return sub;
	}
	
	public BlockNode getNext() {
		return next;
	}
	
	public StmtNode getStmt() {
		return stmt;
	}
	
    public String toString() {
        String s = new String();

        ConstNode cn = cons;

        s += "[BlockNode: " + (name != null ? name : "RootNode") + "]\n";
        s += "\t(CONSTANTS)\n";

        while (cn != null) {
            s += "\t\t" + cn + "\n";
            cn = cn.getNext();
        }

        s += "\n\t(Variable count)\n";
        s += "\t\tCount: " + varCount;

        return s;
    }

}
