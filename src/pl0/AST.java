package pl0;

import java.util.TreeMap;


public class AST {
    
    private BlockNode root;
    private TreeMap<String, BlockNode> procedures;

    public AST(BlockNode root, TreeMap<String, BlockNode> procedures) {
        this.root = root;
        this.procedures = procedures;
    }

    public BlockNode getRoot() {
        return root;
    }

    public TreeMap<String, BlockNode> getProcedures() {
        return this.procedures;
    }
    
}
