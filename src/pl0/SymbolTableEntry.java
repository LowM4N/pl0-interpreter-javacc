package pl0;


public class SymbolTableEntry {

    public static enum Type {
        UNKNOWN,
        CONST,
        VAR,
        PROC
    }

    private Integer level;
    private Integer offset;
    private Type type;

    public SymbolTableEntry() {
        this(-1, -1, Type.UNKNOWN);
    }
    
    public SymbolTableEntry(Integer level, Integer offset, Type type) {
        this.level = level;
    	this.type = type;
    	this.offset = offset;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Integer getLevel() {
        return level;
    }

    public Integer getOffset() {
        return offset;
    }

    public Type getType() {
        return type;
    }

    public String toString() {
        return "Level: " + level + ", Offset: " + offset + ", Type: " + type;
    }
    
}
