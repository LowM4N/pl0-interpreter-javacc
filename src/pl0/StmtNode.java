package pl0;


public class StmtNode {
    public static enum Type {
        ASSIGN,
        CALL,
        INPUT,
        OUTPUT,
        IF,
        WHILE,
        DEBUG
    }

    private String name;
    private Type type;
    private SymbolTableEntry entry;
    private ExprNode expr;
    private StmtNode sub;
    private StmtNode next;

    public StmtNode(String name, Type type, SymbolTableEntry entry) {
        this(name, type, entry, null);
    }

    public StmtNode(String name, Type type, SymbolTableEntry entry, ExprNode expr, StmtNode sub) {
        this(name, type, entry, expr);
        this.sub = sub;
    }

    public StmtNode(String name, Type type, SymbolTableEntry entry, ExprNode expr) {
        this.name = name;
        this.type = type;
        this.entry = entry;
        this.expr = expr;
        this.next = null;
        this.sub = null;
    }
	
	public void setNext(StmtNode next) {
		this.next = next;
	}

	public String getName() {
		return name;
	}
	
	public Type getType() {
		return type;
	}
	
	public SymbolTableEntry getEntry() {
		return entry;
	}
	
	public ExprNode getExpr() {
		return expr;
	}
	
	public StmtNode getNext() {
		return next;
	}
	
	public StmtNode getSub() {
		return sub;
	}

    public String toString() {
        String s = new String();

        s += "[StmtNode: " + name + "]\n";
        s += "\tType: " + type + "\n";
        s += "\tentry: " + entry + "\n";
        s += "\tExpr: " + (expr != null ? expr.getName() : null) + "\n";
        s += "\tNext: " + (next != null ? next.getName() : null) + "\n";
        s += "\tSub: " + (sub != null ? sub.getName() : null) + "\n";

        return s;
    }
}
