package pl0;


public class ExprNode {
    public static enum Type {
        ODD,
        EQ,
        NE,
        LT,
        LE,
        GT,
        GE,
        IDENT,
        NUMBER,
        ADD,
        SUB,
        MUL,
        DIV,
        CS
    }

    private String name;
    private Type type;
    private Integer value;
    private SymbolTableEntry entry;
    private ExprNode left;
    private ExprNode right;

    public ExprNode(String name, Type type, ExprNode left) {
        this(name, type, left, null);
    }

    public ExprNode(String name, Type type, SymbolTableEntry entry) {
        this(name, type, null, null);
        this.entry = entry;
    }

    public ExprNode(String name, Type type, Integer value) {
        this(name, type, null, null);
        this.value = value;
    }

    public ExprNode(String name, Type type, ExprNode left, ExprNode right) {
        this.name = name;
        this.type = type;
        this.left = left;
        this.right = right;
        this.value = null;
        this.entry = null;
    }
	
	public String getName() {
		return name;
	}
	
	public Type getType() {
		return type;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public SymbolTableEntry getEntry() {
		return entry;
	}
	
	public ExprNode getLeft() {
		return left;
	}
	
	public ExprNode getRight() {
		return right;
	}
	
    public String toString() {
        String s = new String();

        s += "[ExprNode: " + name + "]\n";
        s += "\tType: " + type + "\n";
        s += "\tValue: " + value + "\n";
        s += "\tEntry: " + entry + "\n";
        s += "\tLeft: " + (left != null ? left.name : null) + "\n";
        s += "\tRight: " + (right != null ? right.name : null) + "\n";

        return s;
    }
}
