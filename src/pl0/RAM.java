package pl0;


public class RAM {

    private Integer[] ram;
    private Integer activeRecord;

    public RAM(){
        // Init RAM
        ram = new Integer[1024];
        // Set top of stack to zero
        ram[0] = 0;
        // space where to save dynamik and static link
        activeRecord = 2;
    }

    public void addSegment(Integer varCount, Integer level) {
        try {
            // Saves dynamik-link in RAM
            ram[ ram[0] + varCount + activeRecord - 1 ] = ram[0];
            
            int address = ram[0];
            
            // Move throught stack until fitting level is reached
            for(int i = 0; i < level; i++)
                address = ram[address];
            
            // Save static-link in RAM 
            ram[ ram[0] + varCount + activeRecord ] = address;
            // Save new top of stack
            ram[0] = ram[0] + varCount + activeRecord;
        } catch (Exception e) {
            System.out.println("===[ Error, RAM is to small, maximum size is: " + ram.length + " ]===");
            System.exit(0);
        }
    }

    public void removeSegment() {
        // changes top of stack in previus one
        ram[0] = ram[ ram[0] - 1 ];
    }

    public void setValue(SymbolTableEntry entry, Integer value) {
        // searches adress and saves value
        ram[ getAddress(entry) ] = value;
    }

    public Integer getValue(SymbolTableEntry entry) {
        // returns value of adress
        return ram[ getAddress(entry) ];
    }

    private Integer getAddress(SymbolTableEntry entry) {
        int address = ram[0];

        // move throught stack till fitting level is reached
        for (int i = 0; i < entry.getLevel(); i++)
            address = ram[address];

        address -= activeRecord;
        address -= entry.getOffset();

        return address;
    }

    public String toString(){
        String s = new String();
        for(int i = ram[0]; i >= 0 ; i--)
            s += "RAM[" + i + "] = " +  ram[i] + (i != 0 ? "\n" : "");

        return s;
    }

}
